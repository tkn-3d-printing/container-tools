3d printing image
=================

.. _OpenSCAD: http://www.openscad.org/
.. _Slic3r: http://slic3r.org/

Builds and runs a debian:stretch based image with OpenSCAD_ and Slic3r_ in it.
It should include all necessary libraries (and maybe some more :laughing:).
Please note you can start the programs as many times you want. However, a new 
container will be created for each.

Make targets
------------

build
	Builds a new image. 

clean
	Removes an existing image.

start_openscad
	Checks if a suidable image exists. If so it starts OpenSCAD_ in a container. 

start_slic3r
	Checks if a suidable image exists. If so it starts Slic3r_ in a container.
